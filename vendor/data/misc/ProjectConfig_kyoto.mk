# Only item in $(MTK_TARGET_PROJECT_FOLDER)/ProjectConfig.mk con be overlay here
MTK_AUDIO_SPEAKER_PATH = smartpa_awinic_aw882xx
MTK_AURISYS_PHONE_CALL_SUPPORT = no
MTK_HAC_SUPPORT = yes
MTK_VOIP_NORMAL_DMNR = yes
# enable MTK GVA
MTK_VOW_SUPPORT = yes
MTK_VOW_BARGE_IN_SUPPORT = yes
MTK_VOW_DUAL_MIC_SUPPORT = yes
MTK_VOW_MAX_PDK_NUMBER = 0
MTK_VOW_AMAZON_SUPPORT = no
MTK_VOW_GVA_SUPPORT = yes
MTK_VOW_COMMAND = none
MTK_VOW_COMMAND_2ND = none
MTK_VOW_COMMAND_3ND = none
MTK_VOW_DSP_VERSION = ver02
CUSTOM_HAL_IMGSENSOR = mot_s5khm2_mipi_raw mot_ov32b40_mipi_raw mot_ov02b1b_mipi_raw mot_s5k4h7_mipi_raw
CUSTOM_KERNEL_IMGSENSOR = mot_s5khm2_mipi_raw mot_ov32b40_mipi_raw mot_ov02b1b_mipi_raw mot_s5k4h7_mipi_raw
MTK_CAM_MAX_NUMBER_OF_CAMERA = 6
MTK_CAM_STEREO_CAMERA_SUPPORT = yes
MTK_CAM_VSDOF_SUPPORT = yes
MTK_CAM_MFB_SUPPORT = 3
MTK_CAM_MFB_MAX_FRAME = 6
# MTK_CAM_DUAL_ZOOM_SUPPORT = yes
# Enable fingerprint
MTK_FINGERPRINT_SUPPORT = yes
MTK_FINGERPRINT_SELECT = no
# disable unused sensor
CUSTOM_KERNEL_BAROMETER = no
CUSTOM_KERNEL_WAKE_GESTURE_SENSOR = no
CUSTOM_HIFI_SENSORS = no
# Mot duxu1 2021.2.1 add: for nfc support
MTK_NFC_GSMA_SUPPORT = yes
MTK_OMAPI_SUPPORT = yes
LCM_HEIGHT = 2400
LCM_WIDTH = 1080
MTK_AAL_SUPPORT = no
MOTO_CHARGE_ONLY_MODE_ASSET_COLOR = fhdpluse
MOTO_POWEROFF_CHARGING_UI = yes
MOTO_POWEROFF_CHARGING_UI_FHDPLUSE = yes

#Moto configure second modem path
CUSTOM_SECOND_MODEM=mt6853_kyoto_ind
MOTO_SUPER_MODEM_SUPPORT = yes

# Turn off MTK_TELEPHONY_ADD_ON_POLICY
MTK_TELEPHONY_ADD_ON_POLICY = 1

# According to OD, WFD is NOT supported on Saipan / Kyoto / Tonga, IKSWQ-148414
MTK_WFD_SUPPORT = no
MTK_SLOW_MOTION_VIDEO_SUPPORT = yes
